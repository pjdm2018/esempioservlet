package it.uniroma2.pjdm;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.almworks.sqlite4java.*;
import com.google.gson.*;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/student")
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String DBNAME = "students.db";
	private SQLiteConnection db;
	private SQLiteQueue queue;
       
    /**
     * @throws SQLiteException 
     * @see HttpServlet#HttpServlet()
     */
    public StudentServlet() throws SQLiteException {
    	// initialize the database connection
        File dbFile = new File(DBNAME);
        db = new SQLiteConnection(dbFile);
        db.open(true);
        
        // create the database schema if needed
        String SQL_CREATE = 
        		"CREATE TABLE IF NOT EXISTS students (matr TEXT PRIMARY KEY, name TEXT, surname TEXT)";
        db.exec(SQL_CREATE);
        
        // initialize the queue, needed by sqlite4java to support multithreading
        queue = new SQLiteQueue(dbFile);
        queue.start();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 * 
	 * retrieve a student.
	 * To test create a GET request using the "matr" parameter.
	 *  
	 * e.g. http://localhost:8080/EsempioServlet/student?matr=1234
	 * 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get the parameter from the request
		String matr = request.getParameter("matr");
		
		// query the database by creating a job
		String SQL_SELECT = "SELECT matr, name, surname FROM students WHERE matr = ?";
		
		SQLiteJob<Student> sqlJob = new SQLiteJob<Student>() {
			@Override
			protected Student job(SQLiteConnection conn) throws Throwable {		
				// prepare the statement
				SQLiteStatement statement = conn.prepare(SQL_SELECT);
				statement.bind(1, matr);
				
				// retrieve the result(s)
				Student student = new Student();
				while(statement.step()) {
					student.setMatr(statement.columnString(0));
					student.setName(statement.columnString(1));
					student.setSurname(statement.columnString(2));
				}
				log("retrieved from the DB the student " + student.getMatr());
				return student;
			}
		};			
		queue.execute(sqlJob);
		
		// wait for the job to complete
		Student student = sqlJob.complete();

		// return a JSON representation of the retrieved object to the user
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		Gson g = new Gson();
		out.print(g.toJson(student));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * 
	 *  Create a student.
	 *  To test create a POST request to e.g. http://localhost:8080/EsempioServlet/student
	 *  with the followng raw body:
	 *  
	 *  {
     *  "matr": "1234",
     *  "name": "Mario",
     *  "surname": "Rossi"
     *  }
	 * 
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// read the JSON from the user
		BufferedReader br = request.getReader();
		String arg = "";
		String line = br.readLine();
		while(line != null) {
			arg += line;
			line = br.readLine();
		}
		
		log("JSON: " + arg);
		
		// build a Student object from the supplied JSON
		Gson g = new Gson();
		Student student = g.fromJson(arg, Student.class);
		
		// insert into the database
		String SQL_INSERT =
				"INSERT INTO students (matr, name, surname) VALUES (?, ?, ?)";
		
		// create a job object and queue it
		SQLiteJob<Void> sqljob = new SQLiteJob<Void>() {
			@Override
			protected Void job(SQLiteConnection conn) throws Throwable {
				SQLiteStatement statement = conn.prepare(SQL_INSERT);
				statement.bind(1, student.getMatr());
				statement.bind(2, student.getName());
				statement.bind(3, student.getSurname());
				statement.step();
				return null;
			}	
		};
		queue.execute(sqljob);
		
		// notify the user that things look good
		response.setContentType("application/json");
		JsonObject jout = new JsonObject();
		jout.addProperty("result", "OK");
		response.getWriter().println(jout.toString());
	}

}










