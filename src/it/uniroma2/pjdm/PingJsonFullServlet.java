package it.uniroma2.pjdm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.*;


/**
 * Servlet implementation class PingJsonFullServlet
 */
@WebServlet("/pingjsonfull")
public class PingJsonFullServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		// read the full body of the POST
		BufferedReader br = request.getReader();
		String arg = "";
		String line = br.readLine();
		while(line != null) {
			arg += line;
			line = br.readLine();
		}
		// the JSON from the user is now in arg
		
		JsonObject jin = new JsonParser().parse(arg).getAsJsonObject();
		String ping = jin.get("ping").getAsString();
		
		JsonObject jout = new JsonObject();
		jout.addProperty("pong", ping);
		
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.println(jout.toString());
		
	}

}












